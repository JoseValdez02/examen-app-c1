package com.example.examenc1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class rectanguloActivity extends AppCompatActivity {

    private EditText txtBase, txtAltura;
    private TextView lblNombre;
    private TextView lblCalArea, lblCalPerimetro;
    private Button btnCalcular, btnLimpiar, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_rectangulo);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });


        lblNombre = findViewById(R.id.lblNombre);
        txtBase = findViewById(R.id.txtBase);
        txtAltura = findViewById(R.id.txtAltura);
        lblCalArea = findViewById(R.id.lblCalArea);
        lblCalPerimetro = findViewById(R.id.lblCalPerimetro);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSalir = findViewById(R.id.btnSalir);

        String nombre = getIntent().getStringExtra("cliente");
        lblNombre.setText("Nombre:" + nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcular() {
        String baseStr = txtBase.getText().toString();
        String alturaStr = txtAltura.getText().toString();

        if (baseStr.isEmpty() || alturaStr.isEmpty()) {
            Toast.makeText(this, "Por favor ingrese la base y la altura", Toast.LENGTH_SHORT).show();
            return;
        }

        int base = Integer.parseInt(baseStr);
        int altura = Integer.parseInt(alturaStr);

        Rectangulo rectangulo = new Rectangulo(base, altura);

        float area = rectangulo.calcularArea();
        float perimetro = rectangulo.calcularPerimetro();

        lblCalArea.setText(String.format(" %.2f", area));
        lblCalPerimetro.setText(String.format(" %.2f", perimetro));
    }

    private void limpiar() {
        txtBase.setText("");
        txtAltura.setText("");
        lblCalArea.setText("");
        lblCalPerimetro.setText("");
    }
}
